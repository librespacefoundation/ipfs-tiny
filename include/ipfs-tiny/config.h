/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_CONFIG_H_
#define INCLUDE_IPFS_TINY_CONFIG_H_

/**
 * Set to 1 for an increased verbosity on the exceptions or 0 to disable
 * any produced message. For MCU environments this should be set to 0 to reduce
 * the memory footprint
 */
#define IPFS_EXCEPT_VERBOSE 1

/**
 * Maximum message length of the exception.
 * This can be used to minimize the memory footprint for memory limited systems
 * Has no effect of IPFS_EXCEPT_VERBOSE == 0
 */
#define IPFS_EXCEPT_MAX_MSG_LEN 64

#endif /* INCLUDE_IPFS_TINY_CONFIG_H_ */
