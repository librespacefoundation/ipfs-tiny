/* SPDX-License-Identifier: GPL-3.0-or-later */

#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include "test_base16.hpp"
#include "test_base32.hpp"
#include "test_base58.hpp"
#include "test_cid.h"
#include "test_hash.hpp"
#include "test_multibase.hpp"
#include "test_multicodec.hpp"
#include "test_multihash.hpp"
#include "test_nanopb.hpp"
#include "test_storage_interface.hpp"
#include "test_varint.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(test_hash);
CPPUNIT_TEST_SUITE_REGISTRATION(test_base58);
CPPUNIT_TEST_SUITE_REGISTRATION(test_varint);
CPPUNIT_TEST_SUITE_REGISTRATION(test_multihash);
CPPUNIT_TEST_SUITE_REGISTRATION(test_base16);
CPPUNIT_TEST_SUITE_REGISTRATION(test_multibase);
CPPUNIT_TEST_SUITE_REGISTRATION(test_cid);
CPPUNIT_TEST_SUITE_REGISTRATION(test_multicodec);
CPPUNIT_TEST_SUITE_REGISTRATION(test_base32);
CPPUNIT_TEST_SUITE_REGISTRATION(test_nanopb);
CPPUNIT_TEST_SUITE_REGISTRATION(test_storage_interface);

int
main(int argc, char **argv)
{
  CppUnit::TestResult                controller;
  CppUnit::TestResultCollector       result;
  CppUnit::BriefTestProgressListener progressListener;
  CppUnit::TextUi::TestRunner        runner;
  CppUnit::TestFactoryRegistry &     registry =
      CppUnit::TestFactoryRegistry::getRegistry();
  runner.addTest(registry.makeTest());
  runner.eventManager().addListener(&result);
  runner.eventManager().addListener(&progressListener);
  bool success = runner.run("", false);
  return success ? 0 : 1;
}
