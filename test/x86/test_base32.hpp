/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef TEST_X86_TEST_BASE32_HPP_
#define TEST_X86_TEST_BASE32_HPP_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class test_base32 : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_base32);
  CPPUNIT_TEST(enc);
  CPPUNIT_TEST(enc_str);
  CPPUNIT_TEST(dec);
  CPPUNIT_TEST(dec_str);
  CPPUNIT_TEST(full_random);
  CPPUNIT_TEST(full_random_iter);
  CPPUNIT_TEST(full_random_str);
  CPPUNIT_TEST_SUITE_END();

public:
  void
  enc();

  void
  enc_str();

  void
  dec();

  void
  dec_str();

  void
  full_random();

  void
  full_random_iter();

  void
  full_random_str();
};

#endif /* TEST_X86_TEST_BASE32_HPP_ */
