/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_MULTIBASE_BASE16_HPP_
#define INCLUDE_IPFS_TINY_MULTIBASE_BASE16_HPP_

#include "base.hpp"
#include "etl/format_spec.h"
#include "etl/string_stream.h"
#include "etl/vector.h"
#include <cstdlib>

namespace ipfs_tiny::multiformats
{

class base16_decode_exception : public exception
{
public:
  base16_decode_exception(const char *file, size_t line)
      : exception("base16 decoding error", file, line)
  {
  }
};

/**
 * base16 (multibase code: f) class.
 *
 * @tparam T1 input data datatype
 * @tparam T2 output data datatype
 */
template <typename T1, typename T2> class base16 : public base<T1, T2>
{
public:
  base16() : base<T1, T2>(etl::make_string("base16"), 'f') {}

  /**
   * Encodes \p data
   * @param res the string to append the result
   * @param data the input data
   * @return reference to \p res
   */
  etl::istring &
  encode(etl::istring &res, const etl::ivector<uint8_t> &data)
  {
    return encode(res, data.cbegin(), data.cend());
  }

  etl::istring &
  encode(etl::istring &res, etl::ivector<uint8_t>::const_iterator begin,
         etl::ivector<uint8_t>::const_iterator end)
  {
    etl::format_spec format = etl::format_spec().hex().width(2).fill('0');
    etl::string<2>   text;
    text.clear();
    etl::string_stream stream(text);
    while (begin < end) {
      stream << format << static_cast<uint32_t>(*begin);
      begin++;
      res.append(stream.str());
      text.clear();
    }
    return res;
  }

  etl::ivector<uint8_t> &
  encode(etl::ivector<uint8_t> &res, const etl::ivector<uint8_t> &data)
  {
    return encode(res, data.cbegin(), data.cend());
  }

  etl::ivector<uint8_t> &
  encode(etl::ivector<uint8_t> &               res,
         etl::ivector<uint8_t>::const_iterator begin,
         etl::ivector<uint8_t>::const_iterator end)
  {
    etl::format_spec format = etl::format_spec().hex().width(2).fill('0');
    etl::string<2>   text;
    text.clear();
    etl::string_stream stream(text);
    while (begin < end) {
      stream << format << static_cast<uint32_t>(*begin);
      begin++;
      res.push_back(text[0]);
      res.push_back(text[1]);
      text.clear();
    }
    return res;
  }

  /**
   * Decodes \p data
   * @param res the output vector to append the result
   * @param data the input data
   * @return reference to \p res
   */
  etl::ivector<uint8_t> &
  decode(etl::ivector<uint8_t> &res, const etl::istring &data)
  {
    for (auto i = data.cbegin(); i < data.cend(); i += 2) {
      if (!isxdigit(*i) && !(isxdigit(*(i + 1)))) {
        res.clear();
        throw base16_decode_exception(__FILE__, __LINE__);
      }
      etl::string<2> shex;
      shex.push_back(*i);
      shex.push_back(*(i + 1));
      size_t x = 0;
      x        = strtoul(shex.c_str(), 0, 16);
      res.push_back(static_cast<uint8_t>(x));
    }
    return res;
  }

  /**
   * Decodes a range of data in the range of [\p begin, \p end)
   * @param res the structure to append the result
   * @param begin iterator to the first input data
   * @param end iterator to the last input data
   * @return reference to \p res
   */
  etl::ivector<uint8_t> &
  decode(etl::ivector<uint8_t> &res, etl::istring::const_iterator begin,
         etl::istring::const_iterator end)
  {
    for (auto i = begin; i < end; i += 2) {
      if (!isxdigit(*i) && !(isxdigit(*(i + 1)))) {
        res.clear();
        throw base16_decode_exception(__FILE__, __LINE__);
      }
      etl::string<2> shex;
      shex.push_back(*i);
      shex.push_back(*(i + 1));
      size_t x = 0;
      x        = strtoul(shex.c_str(), 0, 16);
      res.push_back(static_cast<uint8_t>(x));
    }
    return res;
  }

  /**
   * Decodes \p data
   * @param res the output vector to append the result
   * @param data the input data
   * @return reference to \p res
   */
  etl::ivector<uint8_t> &
  decode(etl::ivector<uint8_t> &res, const etl::ivector<uint8_t> &data)
  {
    for (auto i = data.cbegin(); i < data.cend(); i += 2) {
      if (!isxdigit(*i) && !(isxdigit(*(i + 1)))) {
        res.clear();
        throw base16_decode_exception(__FILE__, __LINE__);
      }
      etl::string<2> shex;
      shex.push_back(*i);
      shex.push_back(*(i + 1));
      size_t x = 0;
      x        = strtoul(shex.c_str(), 0, 16);
      res.push_back(static_cast<uint8_t>(x));
    }
    return res;
  }

  /**
   * Decodes a range of data in the range of [\p begin, \p end)
   * @param res the structure to append the result
   * @param begin iterator to the first input data
   * @param end iterator to the last input data
   * @return reference to \p res
   */
  etl::ivector<uint8_t> &
  decode(etl::ivector<uint8_t> &               res,
         etl::ivector<uint8_t>::const_iterator begin,
         etl::ivector<uint8_t>::const_iterator end)
  {
    for (auto i = begin; i < end; i += 2) {
      if (!isxdigit(*i) && !(isxdigit(*(i + 1)))) {
        res.clear();
        throw base16_decode_exception(__FILE__, __LINE__);
      }
      etl::string<2> shex;
      shex.push_back(*i);
      shex.push_back(*(i + 1));
      size_t x = 0;
      x        = strtoul(shex.c_str(), 0, 16);
      res.push_back(static_cast<uint8_t>(x));
    }
    return res;
  }

  size_t
  encode_size(size_t len) const
  {
    return 2 * len;
  }

  size_t
  decode_size(size_t len) const
  {
    if (len % 2) {
      throw exception("invalid message size", __FILE__, __LINE__);
    }
    return len / 2;
  }
};

} // namespace ipfs_tiny::multiformats

#endif /* INCLUDE_IPFS_TINY_MULTIBASE_BASE16_HPP_ */
