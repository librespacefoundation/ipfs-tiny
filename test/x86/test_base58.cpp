/* SPDX-License-Identifier: GPL-3.0-or-later */

#include <cppunit/TestCase.h>
#include <etl/string.h>
#include <etl/vector.h>
#include <iostream>
#include <random>

#include "ipfs-tiny/multiformats/base58.hpp"
#include "test_base58.hpp"

void
string_to_vector_uint8(etl::istring &input, etl::ivector<uint8_t> &output)
{
  for (etl::istring::iterator it = input.begin(); it != input.end(); ++it)
    output.push_back((uint8_t)*it);
}

void
test_base58::enc()
{
  etl::string<4>          small("smol");
  etl::vector<uint8_t, 4> small_vec;
  string_to_vector_uint8(small, small_vec);
  etl::string<16>          medium("smolsmolsmolsmol");
  etl::vector<uint8_t, 16> medium_vec;
  string_to_vector_uint8(medium, medium_vec);
  etl::string<64> large(
      "smolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmol");
  etl::vector<uint8_t, 64> large_vec;
  string_to_vector_uint8(large, large_vec);

  etl::vector<uint8_t, 10> small_enc;
  etl::vector<uint8_t, 30> medium_enc;
  // Prepend content to test function behavior on etl::vector
  medium_enc.push_back('P');
  medium_enc.push_back('R');
  medium_enc.push_back('E');
  medium_enc.push_back('-');
  etl::vector<uint8_t, 130> large_enc;

  ipfs_tiny::multiformats::base58<etl::ivector<uint8_t>, etl::ivector<uint8_t>>
      b;

  b.encode(small_enc, small_vec);
  etl::string<10> small_enc_str(small_enc.begin(), small_enc.end());
  etl::string<10> small_enc_result("3x8LNw");
  CPPUNIT_ASSERT(!small_enc_str.compare(small_enc_result));

  b.encode(medium_enc, medium_vec);
  etl::string<30> medium_enc_str(medium_enc.begin(), medium_enc.end());
  etl::string<30> medium_enc_result("PRE-FFhhW8wZQVs2TcdEgXLr5h");
  CPPUNIT_ASSERT(!medium_enc_str.compare(medium_enc_result));

  b.encode(large_enc, large_vec);
  etl::string<130> large_enc_str(large_enc.begin(), large_enc.end());
  etl::string<130> large_enc_result(
      "3JrKf7EbnsKTme4gX7MCWz3CaVAzoZx8AvrtHuPrw6VywwgkVX9URkgQhBJk1YqSN3vfQ7hD"
      "MpDwHLwubYa9CTPq");
  CPPUNIT_ASSERT(!large_enc_str.compare(large_enc_result));
}

void
test_base58::enc_str()
{
  etl::string<4>          small("smol");
  etl::vector<uint8_t, 4> small_vec;
  string_to_vector_uint8(small, small_vec);

  etl::string<16>          medium("smolsmolsmolsmol");
  etl::vector<uint8_t, 16> medium_vec;
  string_to_vector_uint8(medium, medium_vec);

  etl::string<64> large(
      "smolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmol");
  etl::vector<uint8_t, 64> large_vec;
  string_to_vector_uint8(large, large_vec);

  etl::string<10> small_enc_str;
  etl::string<30> medium_enc_str;
  medium_enc_str.push_back('P');
  medium_enc_str.push_back('R');
  medium_enc_str.push_back('E');
  medium_enc_str.push_back('-');
  etl::string<130> large_enc_str;

  ipfs_tiny::multiformats::base58<etl::ivector<uint8_t>, etl::istring> b;

  b.encode(small_enc_str, small_vec);
  etl::string<10> small_enc_result("3x8LNw");
  CPPUNIT_ASSERT(!small_enc_str.compare(small_enc_result));

  b.encode(medium_enc_str, medium_vec);
  etl::string<30> medium_enc_result("PRE-FFhhW8wZQVs2TcdEgXLr5h");
  CPPUNIT_ASSERT(!medium_enc_str.compare(medium_enc_result));

  b.encode(large_enc_str, large_vec);
  etl::string<130> large_enc_result(
      "3JrKf7EbnsKTme4gX7MCWz3CaVAzoZx8AvrtHuPrw6VywwgkVX9URkgQhBJk1YqSN3vfQ7hD"
      "MpDwHLwubYa9CTPq");
  CPPUNIT_ASSERT(!large_enc_str.compare(large_enc_result));
}

void
test_base58::dec_str()
{
  etl::string<10>  small_enc_str("3x8LNw");
  etl::string<30>  medium_enc_str("FFhhW8wZQVs2TcdEgXLr5h");
  etl::string<100> large_enc_str("3JrKf7EbnsKTme4gX7MCWz3CaVAzoZx8AvrtHuPrw6Vyw"
                                 "wgkVX9URkgQhBJk1YqSN3vfQ7hDMpDwHLwubYa9CTPq");
  etl::vector<uint8_t, 10> small_dec;
  etl::vector<uint8_t, 30> medium_dec;
  medium_dec.push_back('P');
  medium_dec.push_back('R');
  medium_dec.push_back('E');
  medium_dec.push_back('-');
  etl::vector<uint8_t, 100> large_dec;

  ipfs_tiny::multiformats::base58<etl::ivector<uint8_t>, etl::istring> b;

  b.decode(small_dec, small_enc_str);
  etl::string<10> small_dec_str(small_dec.begin(), small_dec.end());
  etl::string<10> small_dec_result("smol");
  CPPUNIT_ASSERT(!small_dec_str.compare(small_dec_result));

  b.decode(medium_dec, medium_enc_str);
  etl::string<30> medium_dec_str(medium_dec.begin(), medium_dec.end());
  etl::string<30> medium_dec_result("PRE-smolsmolsmolsmol");
  CPPUNIT_ASSERT(!medium_dec_str.compare(medium_dec_result));

  b.decode(large_dec, large_enc_str);
  etl::string<100> large_dec_str(large_dec.begin(), large_dec.end());
  etl::string<100> large_dec_result(
      "smolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmol");
  CPPUNIT_ASSERT(!large_dec_str.compare(large_dec_result));
}

void
test_base58::dec()
{
  etl::string<10>          small_enc_str("3x8LNw");
  etl::vector<uint8_t, 10> small_enc_vec;
  string_to_vector_uint8(small_enc_str, small_enc_vec);

  etl::string<30>          medium_enc_str("FFhhW8wZQVs2TcdEgXLr5h");
  etl::vector<uint8_t, 30> medium_enc_vec;
  string_to_vector_uint8(medium_enc_str, medium_enc_vec);

  etl::string<100> large_enc_str("3JrKf7EbnsKTme4gX7MCWz3CaVAzoZx8AvrtHuPrw6Vyw"
                                 "wgkVX9URkgQhBJk1YqSN3vfQ7hDMpDwHLwubYa9CTPq");
  etl::vector<uint8_t, 100> large_enc_vec;
  string_to_vector_uint8(large_enc_str, large_enc_vec);

  etl::vector<uint8_t, 10> small_dec;
  etl::vector<uint8_t, 30> medium_dec;
  medium_dec.push_back('P');
  medium_dec.push_back('R');
  medium_dec.push_back('E');
  medium_dec.push_back('-');
  etl::vector<uint8_t, 100> large_dec;

  ipfs_tiny::multiformats::base58<etl::ivector<uint8_t>, etl::ivector<uint8_t>>
      b;

  b.decode(small_dec, small_enc_vec);
  etl::string<10>          small_dec_result("smol");
  etl::vector<uint8_t, 10> small_dec_result_vec;
  string_to_vector_uint8(small_dec_result, small_dec_result_vec);
  CPPUNIT_ASSERT(small_dec == small_dec_result_vec);

  b.decode(medium_dec, medium_enc_vec);
  etl::string<30>          medium_dec_result("PRE-smolsmolsmolsmol");
  etl::vector<uint8_t, 30> medium_dec_result_vec;
  string_to_vector_uint8(medium_dec_result, medium_dec_result_vec);
  CPPUNIT_ASSERT(medium_dec == medium_dec_result_vec);

  b.decode(large_dec, large_enc_vec);
  etl::string<100> large_dec_result(
      "smolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmolsmol");
  etl::vector<uint8_t, 100> large_dec_result_vec;
  string_to_vector_uint8(large_dec_result, large_dec_result_vec);
  CPPUNIT_ASSERT(large_dec == large_dec_result_vec);
}

void
test_base58::dec_malicious()
{
  etl::vector<uint8_t, 10> small_enc_vec;
  for (int i = 0; i < 10; i++) {
    small_enc_vec.push_back(i);
  }

  etl::vector<uint8_t, 30> medium_enc_vec;
  for (int i = 0; i < 30; i++) {
    medium_enc_vec.push_back(i);
  }

  etl::vector<uint8_t, 100> large_enc_vec;
  for (int i = 0; i < 100; i++) {
    large_enc_vec.push_back(i);
  }

  etl::vector<uint8_t, 10>  small_dec;
  etl::vector<uint8_t, 30>  medium_dec;
  etl::vector<uint8_t, 100> large_dec;

  ipfs_tiny::multiformats::base58<etl::ivector<uint8_t>, etl::ivector<uint8_t>>
      b;

  CPPUNIT_ASSERT_THROW(b.decode(small_dec, small_enc_vec),
                       ipfs_tiny::multiformats::base58_decode_exception);
  CPPUNIT_ASSERT_THROW(b.decode(small_dec, small_enc_vec),
                       ipfs_tiny::multiformats::base58_decode_exception);
  CPPUNIT_ASSERT_THROW(b.decode(large_dec, large_enc_vec),
                       ipfs_tiny::multiformats::base58_decode_exception);
}

void
test_base58::full_random()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);

  const size_t msg_len = 1024;
  ipfs_tiny::multiformats::base58<etl::ivector<uint8_t>, etl::ivector<uint8_t>>
                                                b;
  etl::vector<uint8_t, msg_len>                 raw;
  etl::vector<uint8_t, msg_len>                 raw_output;
  etl::vector<uint8_t, msg_len * 138 / 100 + 1> encoded;

  for (size_t i = 0; i < msg_len; i++) {
    raw.push_back(uni(mt));
  }

  b.encode(encoded, raw);
  b.decode(raw_output, encoded);
  CPPUNIT_ASSERT(etl::equal(raw.cbegin(), raw.cend(), raw_output.cbegin()));
}

void
test_base58::full_random_iter()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);

  const size_t msg_len = 1024;
  ipfs_tiny::multiformats::base58<etl::ivector<uint8_t>, etl::ivector<uint8_t>>
                                                b;
  etl::vector<uint8_t, msg_len>                 raw;
  etl::vector<uint8_t, msg_len>                 raw_output;
  etl::vector<uint8_t, msg_len * 138 / 100 + 1> encoded;

  for (size_t i = 0; i < msg_len; i++) {
    raw.push_back(uni(mt));
  }

  b.encode(encoded, raw);
  b.decode(raw_output, encoded.cbegin(), encoded.cend());
  CPPUNIT_ASSERT(etl::equal(raw.cbegin(), raw.cend(), raw_output.cbegin()));
}

void
test_base58::full_random_str()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);

  const size_t msg_len = 1024;
  ipfs_tiny::multiformats::base58<etl::ivector<uint8_t>, etl::istring> b;
  etl::vector<uint8_t, msg_len>                                        raw;
  etl::vector<uint8_t, msg_len>        raw_output;
  etl::string<msg_len * 138 / 100 + 1> encoded;

  for (size_t i = 0; i < msg_len; i++) {
    raw.push_back(uni(mt));
  }

  b.encode(encoded, raw);
  b.decode(raw_output, encoded);
  CPPUNIT_ASSERT(etl::equal(raw.cbegin(), raw.cend(), raw_output.cbegin()));
}
