/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "test_nanopb.hpp"
#include "dagpb.pb.h"
#include <cppunit/TestCase.h>
#include <pb_encode.h>
#include <random>

#include <iostream>

using namespace std;

void
test_nanopb::test_encode()
{
  uint8_t buffer[128];
  size_t  message_length;
  bool    status;

  PBNode       node1  = PBNode_init_zero;
  pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));

  // node1.Data = {0x12, 0x16, 0x12};

  status = pb_encode(&stream, PBNode_fields, &node1);
  CPPUNIT_ASSERT(status);
  message_length = stream.bytes_written;
}