/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_CID_HPP_
#define INCLUDE_IPFS_TINY_CID_HPP_

#include "etl/vector.h"
#include "ipfs-tiny/exception.hpp"
#include "ipfs-tiny/multiformats.hpp"
#include <ctype.h>
#include <iostream>
#include <string>

namespace multib = ipfs_tiny::multiformats::multibase;

namespace ipfs_tiny
{

class cid_parse_exception : public exception
{
public:
  cid_parse_exception(const char *file, size_t line)
      : exception("cid: Malformed CID", file, line)
  {
  }
};

class cid_internal_exception : public exception
{
public:
  cid_internal_exception(const char *file, size_t line)
      : exception("cid: Internal CID parsing error", file, line)
  {
  }
};

enum class cid_version
{
  v0,
  v1
};

template <const size_t MAX_SIZE = 256> class cid
{
public:
  cid()
      : m_valid(false),
        m_version(cid_version::v1),
        m_multicodec(),
        m_code(multiformats::multicodec::raw)
  {
  }

  cid(const etl::istring &data)
      : m_version(cid_version::v1),
        m_multicodec(),
        m_code(multiformats::multicodec::raw)
  {
    decode<etl::istring>(data);
  }

  template <typename T>
  cid(typename T::const_iterator first, typename T::const_iterator last)
      : m_version(cid_version::v1),
        m_multicodec(),
        m_code(multiformats::multicodec::raw)
  {
    decode<T>(first, last);
  }

  cid(const etl::ivector<uint8_t> &data)
  {
    decode<etl::ivector<uint8_t>>(data);
  }

  template <multiformats::multibase::encoding BASE, cid_version VERSION,
            multiformats::multicodec::code CODEC>
  void
  encode(crypto::hash<etl::ivector<uint8_t>> &h,
         const etl::ivector<uint8_t> &        data)
  {
    /* Store selected base of this CID */
    m_base = BASE;
    /* Store version of this CID */
    m_version = VERSION;
    /* Store codec of this CID */
    m_code = CODEC;
    /* Store hash function code of this CID */
    m_hash_code = h.fn();
    /* Store digest output size of hash function */
    m_hash_size = h.size();
    /* Store the digest */
    multiformats::multihash<etl::ivector<uint8_t>>(h, m_buffer, data);
  }

  void
  to_string(etl::istring &output)
  {
    switch (m_version) {
    /**
     * If CID is v0, then include only the required fields.
     * CID Version: included
     * Multihash: included
     * Multibase: implicit (base58)
     * Multicodec: implicit (dagpb)
     */
    case cid_version::v0: {
      if (m_base != multib::encoding::base58btc &&
          m_code != multiformats::multicodec::code::dag_pb) {
        throw cid_parse_exception(__FILE__, __LINE__);
      }
      m_str_buffer.clear();
      multiformats::varint::encode(m_str_buffer, (uint8_t)m_version);
      m_str_buffer.insert(m_str_buffer.end(), m_buffer.begin(), m_buffer.end());
      multiformats::base58<etl::ivector<uint8_t>, etl::istring> b;
      b.encode(output, m_str_buffer.begin(), m_str_buffer.end());
      break;
    }
    case cid_version::v1: {
      m_str_buffer.clear();
      m_str_buffer.push_back((uint8_t)m_version);
      m_str_buffer.push_back((uint8_t)m_code);
      m_str_buffer.insert(m_str_buffer.end(), m_buffer.begin(), m_buffer.end());
      switch ((const multib::encoding)m_base) {
      case multib::encoding::base58btc: {
        multib::encode<multib::encoding::base58btc, etl::istring,
                       etl::ivector<uint8_t>>(output, m_str_buffer);
        break;
      }
      case multib::encoding::base16: {
        multib::encode<multib::encoding::base16, etl::istring,
                       etl::ivector<uint8_t>>(output, m_str_buffer);
        break;
      }
      default: {
        throw cid_parse_exception(__FILE__, __LINE__);
      }
      }
      break;
    }
    default:
      throw cid_parse_exception(__FILE__, __LINE__);
      break;
    }
  }

  void
  decode(const etl::ivector<uint8_t> &data)
  {
    decode<etl::ivector<uint8_t>>(data.cbegin(), data.cend());
  }

  void
  decode(const etl::istring &data)
  {
    decode<etl::istring>(data.cbegin(), data.cend());
  }

  template <typename T>
  void
  decode(typename T::const_iterator first, typename T::const_iterator last)
  {
    /* Check if contents are string and assume CID base / version / encoding */
    // if (is_string<T>(first, last)) {
    try {
      /* Estimate the CID version*/
      if (last - first == 46 && *first == 'Q' && *(first + 1) == 'm') {
        m_base    = multib::encoding::base58btc;
        m_version = cid_version::v0;
        m_code    = multiformats::multicodec::dag_pb;
      } else {
        /* For CID v1 read base */
        switch ((const multib::encoding)*first) {
        case multib::encoding::base58btc: {
          m_base = multib::encoding::base58btc;
          break;
        }
        case multib::encoding::base16: {
          m_base = multib::encoding::base16;
          break;
        }
        default: {
          throw cid_internal_exception(__FILE__, __LINE__);
        }
        }
      }
      if (m_version == cid_version::v0) {
        try {
          multib::decode<multib::encoding::base58btc, etl::ivector<uint8_t>, T>(
              m_str_buffer, first, last);
        } catch (multiformats::base58_decode_exception e) {
          m_base = multib::encoding::extract;
          throw cid_internal_exception(__FILE__, __LINE__);
        }
      } else {
        try {
          multib::decode<etl::ivector<uint8_t>, T>(m_str_buffer, first, last);
        } catch (multiformats::base16_decode_exception e) {
          throw cid_internal_exception(__FILE__, __LINE__);
        }
        if (m_str_buffer[0] == 0x12) {
          throw cid_parse_exception(__FILE__, __LINE__);
        }
      }
    } catch (cid_internal_exception e) {
      /* If input wasn't string, revert the attempt and insert raw byte array to
       * buffer */
      m_str_buffer.insert(m_str_buffer.begin(), first, last);
    }
    /* If it's CID v0, then buffer is the multihash */
    if (m_str_buffer.size() == 34 && m_str_buffer[0] == 0x12 &&
        m_str_buffer[1] == 0x20) {
      m_buffer.clear();
      m_buffer.insert(m_buffer.begin(), m_str_buffer.begin(),
                      m_str_buffer.end());
    } else {
      m_version = (const cid_version)m_str_buffer[0];
      if (m_version != cid_version::v1) {
        throw cid_parse_exception(__FILE__, __LINE__);
      }
      m_multicodec.decode(m_str_buffer.begin() + 1, m_str_buffer.end());
      m_code =
          (const multiformats::multicodec::code)m_multicodec.get_codec().code();
      if (m_code != multiformats::multicodec::code::dag_pb &&
          m_code != multiformats::multicodec::code::raw) {
        m_valid == false;
      }
      m_buffer.clear();
      m_buffer.insert(m_buffer.begin(), m_str_buffer.begin() + 2,
                      m_str_buffer.end());
    }
  }

  cid_version
  version() const
  {
    return m_version;
  }

  etl::vector<uint8_t, MAX_SIZE>
  multihash_buffer() const
  {
    return m_buffer;
  }

  multib::encoding
  base() const
  {
    return m_base;
  }

  multiformats::multicodec::code
  codec() const
  {
    return m_code;
  }

  bool
  valid() const
  {
    return m_valid;
  }

  friend bool
  operator==(const cid &lhs, const cid &rhs)
  {
    return lhs.version() == rhs.version() && lhs.m_code == rhs.m_code &&
           lhs.m_base == rhs.m_base && lhs.m_buffer == rhs.m_buffer;
  }

  friend bool
  operator!=(const cid &lhs, const cid &rhs)
  {
    return !(lhs == rhs);
  }

private:
  bool                           m_valid;
  cid_version                    m_version;
  etl::vector<uint8_t, MAX_SIZE> m_buffer;
  multiformats::multicodec       m_multicodec;
  multiformats::multicodec::code m_code;
  multib::encoding               m_base;
  size_t                         m_hash_code;
  size_t                         m_hash_size;
  etl::vector<uint8_t, MAX_SIZE> m_str_buffer;

  template <typename InputIterator>
  void
  parse(InputIterator first, InputIterator last)
  {
    InputIterator iter = first;
    /* Get codec */
    iter = m_multicodec.decode(iter);
    /* Get CID version */
    uint64_t res;
    iter =
        multiformats::varint(res, iter, iter + multiformats::varint::max_len);
  }
};

} /* namespace ipfs_tiny */

#endif /* INCLUDE_IPFS_TINY_CID_HPP_ */
