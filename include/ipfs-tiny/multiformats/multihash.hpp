/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_MULTIFORMATS_MULTIHASH_HPP_
#define INCLUDE_IPFS_TINY_MULTIFORMATS_MULTIHASH_HPP_

#include "ipfs-tiny/crypto/hash.hpp"
#include "ipfs-tiny/multiformats/varint.hpp"

namespace ipfs_tiny::multiformats
{

template <typename T>
T &
multihash(crypto::hash<T> &h, T &res, const T &data)
{
  varint::encode(res, h.fn());
  varint::encode(res, h.size() / 8);
  h.digest(res, data);
  return res;
}

template <typename T>
T &
multihash(crypto::hash<T> &h, T &res, typename T::const_iterator first,
          typename T::const_iterator last)
{
  varint::encode(res, h.fn());
  varint::encode(res, h.size() / 8);
  h.digest(res, first, last);
  return res;
}

} // namespace ipfs_tiny::multiformats

#endif /* INCLUDE_IPFS_TINY_MULTIFORMATS_MULTIHASH_HPP_ */
