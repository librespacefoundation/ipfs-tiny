# SPDX-License-Identifier: GPL-3.0-or-later

list(APPEND TEST_SOURCES
	test_base16.cpp
	test_base32.cpp
	test_base58.cpp
	test_cid.cpp
	test_hash.cpp
	test_multibase.cpp
	test_multihash.cpp
	test_varint.cpp
	test_x86.cpp
	test_multicodec.cpp
	test_nanopb.cpp
	test_storage_interface.cpp
)

# Generate the .c bindings from the given .ptoto files

include_directories(${NANOPB_INCLUDE_DIRS})
nanopb_generate_cpp(PROTO_SRCS PROTO_HDRS ${PROJECT_SOURCE_DIR}/proto/dagpb.proto)
list(APPEND TEST_SOURCES
	${PROTO_SRCS}
)

add_executable(test-x86 ${TEST_SOURCES})

target_include_directories(test-x86 PUBLIC
	${CPPUNIT_INCLUDE_DIRS}
	"${PROJECT_BINARY_DIR}/etl/include"
	"${PROJECT_SOURCE_DIR}/etl/include"
	"${PROJECT_BINARY_DIR}/include"
	"${PROJECT_SOURCE_DIR}/include"
	${NANOPB_INCLUDE_DIRS}
	# The nanopb autogenerates the files at the binary directory
	${CMAKE_CURRENT_BINARY_DIR}
)

target_link_libraries(test-x86
    ${CPPUNIT_LIBRARY}
    ipfs-tiny
)

add_test(test-x86 test-x86)
set_tests_properties(test-x86 PROPERTIES TIMEOUT 120)

########################################################################
# Code coverage testing
########################################################################
if(ENABLE_COVERAGE)
	set(COVERAGE_EXCLUDES
        ${PROJECT_SOURCE_DIR}/etl/*
        ${PROJECT_SOURCE_DIR}/test/*)

    setup_target_for_coverage_gcovr_html(
         NAME coverage-x86-html
         EXECUTABLE test-x86
         DEPENDENCIES test-x86
         EXCLUDE  "${PROJECT_SOURCE_DIR}/etl/*" "${PROJECT_SOURCE_DIR}/test/*")
    setup_target_for_coverage_gcovr_xml(
         NAME coverage-x86-xml
         EXECUTABLE test-x86
         DEPENDENCIES test-x86
         EXCLUDE  "${PROJECT_SOURCE_DIR}/etl/*" "${PROJECT_SOURCE_DIR}/test/*")
endif()


