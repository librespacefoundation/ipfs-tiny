/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef TEST_X86_TEST_NANOPB_HPP_
#define TEST_X86_TEST_NANOPB_HPP_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class test_nanopb : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_nanopb);
  CPPUNIT_TEST(test_encode);
  CPPUNIT_TEST_SUITE_END();

public:
  void
  test_encode();
};

#endif /* TEST_X86_TEST_NANOPB_HPP_ */
