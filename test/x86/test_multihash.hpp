/*
 * test_multihash.hpp
 *
 *  Created on: Mar 28, 2021
 *      Author: surligas
 */

#ifndef TEST_X86_TEST_MULTIHASH_HPP_
#define TEST_X86_TEST_MULTIHASH_HPP_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class test_multihash : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_multihash);
  CPPUNIT_TEST(multiformat_correct);
  CPPUNIT_TEST(multiformat_incorrect);
  CPPUNIT_TEST_SUITE_END();

public:
  void
  multiformat_correct();

  void
  multiformat_incorrect();
};

#endif /* TEST_X86_TEST_MULTIHASH_HPP_ */
