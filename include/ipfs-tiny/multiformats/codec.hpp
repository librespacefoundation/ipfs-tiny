/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_MULTIFORMATS_CODEC_HPP_
#define INCLUDE_IPFS_TINY_MULTIFORMATS_CODEC_HPP_

#include <etl/string.h>

namespace ipfs_tiny::multiformats
{

class codec
{
public:
  codec(const etl::istring &name, uint32_t code);

  ~codec() {}

  uint32_t
  code() const;

  const etl::istring &
  name() const;

  friend bool
  operator==(const codec &lhs, const codec &rhs);

private:
  const etl::string<32> m_name;
  const uint32_t        m_code;
};

} // namespace ipfs_tiny::multiformats

#endif /* INCLUDE_IPFS_TINY_MULTIFORMATS_CODEC_HPP_ */
