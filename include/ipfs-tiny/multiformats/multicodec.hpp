/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_MULTIFORMATS_MULTICODEC_HPP_
#define INCLUDE_IPFS_TINY_MULTIFORMATS_MULTICODEC_HPP_

#include "codec.hpp"
#include "ipfs-tiny/exception.hpp"
#include "varint.hpp"
#include <etl/map.h>
#include <etl/string.h>

namespace ipfs_tiny::multiformats
{

class multicodec_uknown_codec : public exception
{
public:
  multicodec_uknown_codec(const char *file, size_t line)
      : exception("multicodec: Unknown codec", file, line)
  {
  }
};

class multicodec_invalid_state : public exception
{
public:
  multicodec_invalid_state(const char *file, size_t line)
      : exception("multicodec: Object has invalid state", file, line)
  {
  }
};

class multicodec
{
public:
  enum code : uint64_t
  {
    raw    = 0x55,
    dag_pb = 0x70
  };

  multicodec()
      : m_codecs{{0x55, codec(etl::make_string("raw"), 0x55)},
                 {0x70, codec(etl::make_string("dag-pb"), 0x70)}},
        m_code(0),
        m_valid(false)
  {
  }

  multicodec(code c)
      : m_codecs{{0x55, codec(etl::make_string("raw"), 0x55)},
                 {0x70, codec(etl::make_string("dag-pb"), 0x70)}},
        m_code(c),
        m_valid(true)
  {
  }

  void
  set_code(uint64_t code)
  {
    m_code  = code;
    m_valid = false;
    if (m_code == raw || m_code == dag_pb) {
      m_valid = true;
    }
  }

  const codec &
  get_codec() const
  {
    if (!m_valid) {
      throw multicodec_invalid_state(__FILE__, __LINE__);
    }

    return m_codecs.at(m_code);
  }

  bool
  valid() const
  {
    return m_valid;
  }

  friend bool
  operator==(const multicodec &lhs, const multicodec &rhs)
  {
    return lhs.get_codec() == rhs.get_codec();
  }

  template <typename InputIterator>
  InputIterator
  decode(InputIterator first, InputIterator last)
  {
    uint64_t res;
    first = varint::decode(res, first, last);
    try {
      m_codecs.at(res);
      m_code  = res;
      m_valid = true;
    } catch (etl::map_out_of_bounds &e) {
      throw multicodec_uknown_codec(__FILE__, __LINE__);
    }
    return first;
  }

  template <typename EncodedT>
  EncodedT &
  encode(EncodedT &res)
  {
    if (!m_valid) {
      throw multicodec_invalid_state(__FILE__, __LINE__);
    }

    return varint::encode(res, m_code);
  }

  template <uint64_t code, typename InputIterator>
  static codec
  decode(InputIterator first, InputIterator last)
  {
    uint64_t res;
    first = varint::decode(res, first, last);

    switch (code) {
    case raw:
      return codec(etl::make_string("raw"), raw);
    case dag_pb:
      return codec(etl::make_string("dag-pb"), dag_pb);
    default:
      // cannot reach this point
      throw multicodec_uknown_codec(__FILE__, __LINE__);
    }
  }

  template <uint64_t code, typename EncodedT>
  static codec
  encode(EncodedT &res)
  {
    varint::encode(res, code);

    switch (code) {
    case raw:
      return codec(etl::make_string("raw"), raw);
    case dag_pb:
      return codec(etl::make_string("dag-pb"), dag_pb);
    default:
      // cannot reach this point
      throw multicodec_uknown_codec(__FILE__, __LINE__);
    }
  }

private:
  const etl::map<uint64_t, const codec, 2> m_codecs;
  uint64_t                                 m_code;
  bool                                     m_valid;
};

} // namespace ipfs_tiny::multiformats

#endif /* INCLUDE_IPFS_TINY_MULTIFORMATS_MULTICODEC_HPP_ */
