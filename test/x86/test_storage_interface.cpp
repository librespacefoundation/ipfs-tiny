/* SPDX-License-Identifier: GPL-3.0-or-later */

#include "test_storage_interface.hpp"
#include <etl/string.h>
#include <etl/vector.h>

using namespace std;

void
test_storage_interface::test_basic()
{
  ipfs_tiny::storage *storageDevice;
  dummyStorage *      s = new dummyStorage();
  storageDevice         = s;
  etl::string<10> node1("Qm1111");
  etl::string<10> node2("Qm2222");
  etl::string<10> node3("Qm3333");
  etl::string<10> node4("Qm4444");

  etl::vector<uint8_t, 6> block1{1, 1, 1, 1, 1, 1};
  etl::vector<uint8_t, 6> block2{2, 1, 1, 1, 2, 1};
  etl::vector<uint8_t, 6> block3{3, 1, 1, 1, 3, 1};
  etl::vector<uint8_t, 6> block4{4, 1, 1, 1, 4, 1};

  etl::vector<uint8_t, 6> temp;
  temp.clear();
  s->store(node1, block1);
  CPPUNIT_ASSERT(s->read(node1, temp));
  temp.clear();
  CPPUNIT_ASSERT(!s->read(node2, temp));
  temp.clear();
  s->store(node2, block2);
  CPPUNIT_ASSERT(s->read(node2, temp));
  temp.clear();
  s->store(node3, block3);
  s->store(node4, block4);
  s->read(node2, temp);
  temp.clear();
  CPPUNIT_ASSERT(std::equal(block2.begin(), block2.end(), temp.begin()));
  s->read(node4, temp);
  temp.clear();
  CPPUNIT_ASSERT(!std::equal(block3.begin(), block3.end(), temp.begin()));
  CPPUNIT_ASSERT(s->erase(node3));
  CPPUNIT_ASSERT(!s->read(node3, temp));
  CPPUNIT_ASSERT(s->read(node4, temp));
  CPPUNIT_ASSERT(s->erase(node1));
  CPPUNIT_ASSERT(s->erase(node2));
  CPPUNIT_ASSERT(s->erase(node4));
  CPPUNIT_ASSERT(!s->read(node1, temp));
  CPPUNIT_ASSERT(!s->read(node2, temp));
  CPPUNIT_ASSERT(!s->read(node3, temp));
  CPPUNIT_ASSERT(!s->read(node4, temp));
}
