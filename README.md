[TOC]

# IPFS-tiny

This project aims to bring IPFS/IPLD closer to embedded devices. As a library it maintains OS independence, allowing it to run on any architecture that C++ compilers can target.

## Channels
Join us on matrix, [#ipfs-tiny](https://matrix.to/#/!EwkkgdChnKDeNpefux:matrix.org?via=matrix.org)

## Building

### Install Dependencies:
```
git submodule update --init
```

Included submodules:
- [ETL](https://github.com/ETLCPP/etl)
- [NanoPB](https://github.com/nanopb/nanopb)

System Dependencies:
- cmake
- libcppunit-dev

### Build
```
cd build
cmake ..
make
```

### Test
```
ctest -VV # flag for verbose output
```

## Contributing

If you want to contribute with code or provide any other form of help, make sure to check the [Contributing Guidelines](./CONTRIBUTING.md)

## License

GNU General Public License v3.0
